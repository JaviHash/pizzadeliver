package com.creative.javier.pizzadeliver.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.creative.javier.pizzadeliver.R;
import com.creative.javier.pizzadeliver.data.Order;

public class Confirm extends AppCompatActivity implements View.OnClickListener{

    private Order order;
    private TextView tvItemName;
    private TextView tvItemSize;
    private TextView tvItemTotal;
    private TextView tvItemAmount;
    private ImageView ivItemImage;
    private Button btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        init();
    }

    private void init(){
        tvItemAmount = findViewById(R.id.tvItemCant);
        tvItemSize = findViewById(R.id.tvItemSize);
        tvItemTotal = findViewById(R.id.tvItemTotal);
        tvItemName = findViewById(R.id.tvItemNameConfirm);
        btnConfirm = findViewById(R.id.btnConfirm);
        ivItemImage = findViewById(R.id.ivItemConfirmProduct);
        order = getIntent().getParcelableExtra("Order");
        fillDataFromOrder(order);
        btnConfirm.setOnClickListener(this);
    }

    private void fillDataFromOrder(Order ord){

        if(ord.isSimple()){
            tvItemName.setText(ord.getSimplePizza().getType());
            tvItemSize.setText(ord.getSimplePizza().getSize());
            tvItemAmount.setText(tvItemAmount.getText()+" "+String.valueOf(ord.getSimplePizza().getAmount()));
            ivItemImage.setImageResource(R.drawable.pizza_logo);
            order.setTotal(ord.getSimplePizza().getPrice());
            if (ord.isHasDessert()){
                order.setTotal(order.getTotal() + ord.getDessert().getPrice());
            }
            if (ord.isHasAppetizer()){
                order.setTotal(order.getTotal() + ord.getAppetizer().getPrice());
            }
            if (ord.isHasDrink()){
                order.setTotal(order.getTotal() + ord.getDrink().getPrice());
            }
            tvItemTotal.setText(tvItemTotal.getText() + " " +String.valueOf(order.getTotal()));
        }
        else{
            if(ord.isHasAppetizer()){
                order.setTotal(ord.getTotal() + ord.getAppetizer().getPrice());
                if(ord.isHasDrink())
                    order.setTotal(order.getTotal() + ord.getDrink().getPrice());
                if(ord.isHasDessert())
                    order.setTotal(order.getTotal() + ord.getDessert().getPrice());
            }
            else if(ord.isHasDessert()){

            }
            else if (ord.isHasDrink()){

            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent next = new Intent(this,Locations.class);
        startActivity(next);
    }
}
