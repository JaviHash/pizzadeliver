package com.creative.javier.pizzadeliver.screens;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.creative.javier.pizzadeliver.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Locations extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener{
    private static final int LOCATION_REQUEST_CODE = 1;
    private GoogleMap mMap;
    private CameraUpdate loc;
    private Marker marker;
    private MarkerOptions options;
    private Geocoder nameLocation;
    private String address;
    private List<Address> addresses;
    private Button btnConfirmLocation;
    private Button btnGo;
    private TextInputEditText edGo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);
        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.frgMaps);
        map.getMapAsync(this);
        init();
    }

    private void init(){
        btnConfirmLocation = findViewById(R.id.btnConfirmLocation);
        btnGo = findViewById(R.id.btnGo);
        edGo = findViewById(R.id.edGo);
        btnConfirmLocation.setOnClickListener(this);
        btnGo.setOnClickListener(this);
    }

    private void initMap(final GoogleMap googleMap){
        mMap = googleMap;
        nameLocation = new Geocoder(this, Locale.getDefault());
        //Check the permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show permission dialog
            } else {
                // Request the permission
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initMap(googleMap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try{
            if (requestCode == LOCATION_REQUEST_CODE) {
                // ¿Permisos asignados?
                if (permissions.length > 0 &&
                        permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION) &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                    // Marcadores
                } else {
                    Toast.makeText(this, "Error de permisos", Toast.LENGTH_LONG).show();
                }

            }
        }catch (SecurityException ex){}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnGo:
                try{
                    String search = edGo.getText().toString();
                    nameLocation = new Geocoder(this, Locale.getDefault());
                    addresses = nameLocation.getFromLocationName(search,1);
                    if (! addresses.isEmpty()){
                        address = addresses.get(0).getThoroughfare() + ", " + addresses.get(0).getLocality() +", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName();
                    }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude()),18.0f));
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    options = new MarkerOptions()
                            .position(new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude()))
                            .title("Mi Ubicacion")
                            .snippet(address);
                    mMap.clear();
                    marker = mMap.addMarker(options);
                }catch (IOException ex){}
            break;
            case R.id.btnConfirmLocation:
                Intent next = new Intent(this, AcceptScreen.class);
                startActivity(next);
                this.finish();
            break;
        }
    }
}

