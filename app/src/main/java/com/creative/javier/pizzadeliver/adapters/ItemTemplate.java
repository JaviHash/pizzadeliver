package com.creative.javier.pizzadeliver.adapters;

import android.graphics.Bitmap;

public class ItemTemplate {
    //This is just for demo
    private int image;
    //This is for release with data from service
    //private Bitmap image;
    private String item;
    private String itemDescription;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }
}
