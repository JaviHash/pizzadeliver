package com.creative.javier.pizzadeliver.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.creative.javier.pizzadeliver.R;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextActivity();
            }
        },4000);
    }

    private void nextActivity(){
        Intent next = new Intent(this, Login.class);
        startActivity(next);
    }
}
