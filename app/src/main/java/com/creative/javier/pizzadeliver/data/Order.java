package com.creative.javier.pizzadeliver.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.creative.javier.pizzadeliver.models.Appetizer;
import com.creative.javier.pizzadeliver.models.Dessert;
import com.creative.javier.pizzadeliver.models.Drink;
import com.creative.javier.pizzadeliver.models.OwnPizza;
import com.creative.javier.pizzadeliver.models.SimplePizza;

public class Order implements Parcelable{
    private int total;

    private boolean isSimple;
    private boolean isOwn;
    private boolean hasDessert;
    private boolean hasAppetizer;
    private boolean hasDrink;

    private OwnPizza ownPizza;
    private SimplePizza simplePizza;
    private Dessert dessert;
    private Drink drink;
    private Appetizer appetizer;

    public Order(){}
    protected Order(Parcel in) {
        isSimple = in.readByte() != 0;
        isOwn = in.readByte() != 0;
        hasDessert = in.readByte() != 0;
        hasAppetizer = in.readByte() != 0;
        hasDrink = in.readByte() != 0;
        total = in.readInt();
        simplePizza = in.readParcelable(SimplePizza.class.getClassLoader());
        appetizer = in.readParcelable(Appetizer.class.getClassLoader());
        dessert = in.readParcelable(Dessert.class.getClassLoader());
        drink = in.readParcelable(Drink.class.getClassLoader());
        ownPizza = in.readParcelable(OwnPizza.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (isSimple ? 1 : 0));
        dest.writeByte((byte) (isOwn ? 1 : 0));
        dest.writeByte((byte) (hasDessert ? 1 : 0));
        dest.writeByte((byte) (hasAppetizer ? 1 : 0));
        dest.writeByte((byte) (hasDrink ? 1 : 0));
        dest.writeInt(total);
        dest.writeParcelable(simplePizza, flags);
        dest.writeParcelable(appetizer, flags);
        dest.writeParcelable(dessert, flags);
        dest.writeParcelable(drink, flags);
        dest.writeParcelable(ownPizza, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public boolean isSimple() {
        return isSimple;
    }

    public void setSimple(boolean simple) {
        isSimple = simple;
    }

    public boolean isOwn() {
        return isOwn;
    }

    public void setOwn(boolean own) {
        isOwn = own;
    }

    public boolean isHasDessert() {
        return hasDessert;
    }

    public void setHasDessert(boolean hasDessert) {
        this.hasDessert = hasDessert;
    }

    public boolean isHasAppetizer() {
        return hasAppetizer;
    }

    public void setHasAppetizer(boolean hasAppetizer) {
        this.hasAppetizer = hasAppetizer;
    }

    public boolean isHasDrink() {
        return hasDrink;
    }

    public void setHasDrink(boolean hasDrink) {
        this.hasDrink = hasDrink;
    }

    public OwnPizza getOwnPizza() {
        return ownPizza;
    }

    public void setOwnPizza(OwnPizza ownPizza) {
        this.ownPizza = ownPizza;
    }

    public SimplePizza getSimplePizza() {
        return simplePizza;
    }

    public void setSimplePizza(SimplePizza simplePizza) {
        this.simplePizza = simplePizza;
    }

    public Dessert getDessert() {
        return dessert;
    }

    public void setDessert(Dessert dessert) {
        this.dessert = dessert;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public Appetizer getAppetizer() {
        return appetizer;
    }

    public void setAppetizer(Appetizer appetizer) {
        this.appetizer = appetizer;
    }

    public int getTotal(){ return total; }

    public void setTotal(int total){
        this.total = total;
    }
}
