package com.creative.javier.pizzadeliver.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.creative.javier.pizzadeliver.R;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public interface IOnClickItemListener{
        void onItemClick(ItemTemplate item);
    }

    private List<ItemTemplate> dataSet;
    private Context context;
    private IOnClickItemListener listener;

    public RecyclerViewAdapter(List<ItemTemplate> dataSet, Context context, IOnClickItemListener listener) {
        this.dataSet = dataSet;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_template_layout,parent,false);
        return new ViewHolder(view) ;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {
        holder.bind(dataSet.get(position),listener);
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView ivItem;
        private TextView tvItem;
        private TextView tvItemDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            tvItem = itemView.findViewById(R.id.tvItem);
            tvItemDescription= itemView.findViewById(R.id.tvItemDescription);
            ivItem = itemView.findViewById(R.id.ivItem);
        }

        public void bind(final ItemTemplate item, final IOnClickItemListener listener){
            ivItem.setImageResource(item.getImage());
            tvItem.setText(item.getItem());
            tvItemDescription.setText(item.getItemDescription());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
