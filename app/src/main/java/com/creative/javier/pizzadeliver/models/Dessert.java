package com.creative.javier.pizzadeliver.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Dessert implements Parcelable{
    private String name;
    private int amount;
    private String size;
    private int price;

    public Dessert(){}
    protected Dessert(Parcel in) {
        name = in.readString();
        amount = in.readInt();
        size = in.readString();
        price = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(amount);
        dest.writeInt(price);
        dest.writeString(size);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Dessert> CREATOR = new Creator<Dessert>() {
        @Override
        public Dessert createFromParcel(Parcel in) {
            return new Dessert(in);
        }

        @Override
        public Dessert[] newArray(int size) {
            return new Dessert[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
