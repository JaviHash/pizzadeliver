package com.creative.javier.pizzadeliver.data;

import android.content.Context;

import com.creative.javier.pizzadeliver.R;
import com.creative.javier.pizzadeliver.adapters.ItemTemplate;

import java.util.ArrayList;
import java.util.List;

public class StaticData {

    public String[] ingredients;

    private String[] bebidas;
    private String[] pizzas;
    private String[] postres;
    private String[] entradas;

    public List<ItemTemplate> fillDemoData(String producto, Context context){

        bebidas = context.getResources().getStringArray(R.array.Bebidas);
        pizzas = context.getResources().getStringArray(R.array.Pizzas);
        postres = context.getResources().getStringArray(R.array.Postres);
        entradas = context.getResources().getStringArray(R.array.Entradas);

        List<ItemTemplate> demo = new ArrayList();
        ItemTemplate item ;
        switch (producto){
            case "bebidas":
                demo = new ArrayList();
                for (int i = 0; i < bebidas.length; i++){
                    item = new ItemTemplate();
                    item.setImage(R.drawable.bebidas);
                    item.setItem(bebidas[i]);
                    item.setItemDescription("Descripcion para demo");
                    demo.add(item);
                }
                break;
            case "pizzas":
                demo = new ArrayList();
                for (int i = 0; i < pizzas.length; i++){
                    item = new ItemTemplate();
                    item.setImage(R.drawable.pizza_logo);
                    item.setItem(pizzas[i]);
                    item.setItemDescription("Descripcion para demo");
                    demo.add(item);
                }
                break;
            case "postres":
                demo = new ArrayList();
                for (int i = 0; i < postres.length; i++){
                    item = new ItemTemplate();
                    item.setImage(R.drawable.postres);
                    item.setItem(postres[i]);
                    item.setItemDescription("Descripcion para demo");
                    demo.add(item);
                }
                break;
            case "entradas":
                demo = new ArrayList();
                for (int i = 0; i < entradas.length; i++){
                    item = new ItemTemplate();
                    item.setImage(R.drawable.entradas);
                    item.setItem(entradas[i]);
                    item.setItemDescription("Descripcion para demo");
                    demo.add(item);
                }
                break;
        }
        return demo;
    }

}
