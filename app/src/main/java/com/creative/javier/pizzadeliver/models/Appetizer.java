package com.creative.javier.pizzadeliver.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Appetizer implements Parcelable{
    private String name;
    private int amount;
    private String size;
    private int price;

    public Appetizer(){}
    protected Appetizer(Parcel in) {
        name = in.readString();
        amount = in.readInt();
        size = in.readString();
        price = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(amount);
        dest.writeInt(price);
        dest.writeString(size);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Appetizer> CREATOR = new Creator<Appetizer>() {
        @Override
        public Appetizer createFromParcel(Parcel in) {
            return new Appetizer(in);
        }

        @Override
        public Appetizer[] newArray(int size) {
            return new Appetizer[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
